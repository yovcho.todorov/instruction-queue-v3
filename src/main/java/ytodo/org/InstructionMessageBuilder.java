package ytodo.org;

public class InstructionMessageBuilder {
    private String instructionType;
    private String productCode;
    private int quantity;
    private int UOM;
    private String timestamp;

    public InstructionMessageBuilder withInstructionType(String instructionType) {
        this.instructionType = instructionType;
        return this;
    }


    public InstructionMessageBuilder withProductCode(String productCode) {
        this.productCode = productCode;
        return this;
    }

    public InstructionMessageBuilder withQuantity(int quantity) {
        this.quantity = quantity;
        return this;
    }

    public InstructionMessageBuilder withUOM(int UOM) {
        this.UOM = UOM;
        return this;
    }

    public InstructionMessageBuilder withTimestamp(String timestamp) {
        this.timestamp = timestamp;
        return this;
    }

    public InstructionMessage build() {
        InstructionMessage message = new InstructionMessage();
        message.setInstructionType(instructionType);
        message.setProductCode(productCode);
        message.setQuantity(quantity);
        message.setUOM(UOM);
        message.setTimestamp(timestamp);
        return message;
    }

}
