package ytodo.org;

public class MessageReceiverImplementation implements MessageReceiver {
    private final InstructionQueue queue;

    public MessageReceiverImplementation(InstructionQueue queue) {
        this.queue = queue;
    }

    @Override
    public void receive(String message) {
        try {
            queue.enqueue(message);
        } catch (Exception e) {
            System.err.println("Failed to enqueue message: " + e.getMessage());
        }
    }
}
