package ytodo.org;

import jdk.jshell.execution.LoaderDelegate;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Date;

public class MessageValidator {

    public static boolean isValidInstructionType(String type) {
        return "A".equals(type) || "B".equals(type) || "C".equals(type) || "D".equals(type);
    }

    public static boolean isValidProductCode(String code) {
        return code.matches("^[A-Z]{2}\\d{2}$");
    }

    public static boolean isValidQuantity(String quantity) {
        try {
            return Integer.parseInt(quantity) > 0;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public static boolean isValidUOM(String uom) {
        try {
            int value = Integer.parseInt(uom);
            return value >= 0 && value < 256;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public static boolean isValidTimestamp(String timestamp) {
        DateTimeFormatter formatter =
                DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        final LocalDateTime UNIX_EPOCH_DATE =
                LocalDateTime.ofInstant(Instant.EPOCH, ZoneOffset.UTC);
        try {
            LocalDateTime date  = LocalDateTime.parse(timestamp, formatter);
            return (!(date.isBefore(UNIX_EPOCH_DATE)) && date.isBefore(LocalDateTime.now()));
        } catch (DateTimeParseException e) {
            return false;
        }
    }

    public  void validateMessage(InstructionMessage message) {
        if(!isValidInstructionType(message.getInstructionType())) {
            throw new IllegalArgumentException("!!!! Invalid instruction type: " + message.getInstructionType());
        }
        if(!isValidProductCode(message.getProductCode())) {
            throw new IllegalArgumentException("!!!! Invalid product code: " + message.getProductCode());
        }
        if(!isValidQuantity(String.valueOf(message.getQuantity()))) {
            throw new IllegalArgumentException("!!!! Invalid quantity: " + message.getQuantity());
        }
        if(!isValidUOM(String.valueOf(message.getUOM()))) {
            throw new IllegalArgumentException("!!!! Invalid UOM: " + message.getUOM());
        }
        if(!isValidTimestamp(message.getTimestamp())) {
            throw new IllegalArgumentException("!!!! Invalid timestamp: " + message.getTimestamp());
        }
    }
}