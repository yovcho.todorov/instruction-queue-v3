package ytodo.org;

public interface MessageReceiver {
    void receive(String message);
}
