package ytodo.org;

import java.util.Comparator;
import java.util.PriorityQueue;

public class InstructionQueue {
    private PriorityQueue<InstructionMessage> queue;

    public InstructionQueue() {
        Comparator<InstructionMessage> comparator = Comparator.comparing(InstructionMessage::getPriority)
                .thenComparing(InstructionMessage::getTimestamp); // Assuming a older timestamp is dequeued first.
        queue = new PriorityQueue<>(comparator);
    }

    public void enqueue(String receivedMessage) throws Exception {
        InstructionMessageBuilder builder = new InstructionMessageBuilder();
        String[] parts = receivedMessage.split(" ");
        if (parts.length < 6) throw new IllegalArgumentException("Invalid message length");

        builder.withInstructionType(parts[1])
                .withProductCode(parts[2])
                .withQuantity(Integer.parseInt(parts[3]))
                .withUOM(Integer.parseInt(parts[4]))
                .withTimestamp(parts[5]);

        InstructionMessage message = builder.build();

        try {
            MessageValidator validator = new MessageValidator();
            validator.validateMessage(message);
            queue.add(message);
            // If applicable, sort your messages according to priority
            // Collections.sort(messages, yourComparator);
        } catch (IllegalArgumentException ex) {
            // Log the error and/or notify the user in an appropriate manner
            System.err.println("Failed to enqueue message: " + ex.getMessage());
        }
    }

    public InstructionMessage dequeue() {
        return queue.poll();
    }

    public InstructionMessage peek(){
        return queue.peek();
    }

    public int count(){
        return queue.size();
    }

    public boolean isEmpty(){
        return queue.isEmpty();
    }
}
