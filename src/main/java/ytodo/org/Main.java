package ytodo.org;

//TIP To <b>Run</b> code, press <shortcut actionId="Run"/> or
// click the <icon src="AllIcons.Actions.Execute"/> icon in the gutter.
public class Main {
    public static void main(String[] args) {
        InstructionQueue instructionQueue = new InstructionQueue();
        InstructionMessageBuilder builder = new InstructionMessageBuilder();
        builder.withInstructionType("A")
                .withProductCode("MZ89")
                .withQuantity(10)
                .withUOM(200)
                .withTimestamp("2024-03-05T10:04:56.012Z");
        InstructionMessage message = builder.build();
        MessageReceiver receiver = new MessageReceiverImplementation(instructionQueue);

        receiver.receive(message.toString());
        receiver.receive("InstructionMessage A KK89 5678 50 2010-03-04T10:04:48.012Z");
        receiver.receive("InstructionMessage A KK89 5678 50 2050-03-04T10:04:48.012Z"); // Invalid timestamp message due to year = 2050

        if (instructionQueue.isEmpty()){
            System.err.println("The queue is empty");
        }else{
            System.out.println("Current number of messages on the queue: " + instructionQueue.count());
            System.out.println("Peek a message on the queue: " + instructionQueue.peek());
            System.out.println("Current number of messages on the queue: " + instructionQueue.count());
            InstructionMessage dequeuedMessage = instructionQueue.dequeue();
            System.out.println("Removing a message from the queue: " + dequeuedMessage);
            System.out.println("Current number of messages on the queue: " + instructionQueue.count());
        }

    }
}