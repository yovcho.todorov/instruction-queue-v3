package ytodo.org;

import java.util.HashMap;
import java.util.Map;

public class InstructionMessage {

    public enum Priority {
        HIGH,
        MEDIUM,
        LOW,
        UNKNOWN;

        private static final Map<String, Priority> PRIORITY_MAP = new HashMap<>();

        static {
            PRIORITY_MAP.put("A", HIGH);
            PRIORITY_MAP.put("B", MEDIUM);
            PRIORITY_MAP.put("C", LOW);
            PRIORITY_MAP.put("D", LOW);
        }

        public static Priority fromInstructionType(String instructionType) {
            return PRIORITY_MAP.getOrDefault(instructionType, UNKNOWN);
        }
    }

    private String instructionType;
    private String productCode;
    private int quantity;
    private int UOM;
    private String timestamp;
    private Priority priority;

    public String getInstructionType() { return instructionType; }
    public String getProductCode() { return productCode; }
    public int getQuantity() { return quantity; }
    public int getUOM() { return UOM; }
    public String getTimestamp() { return timestamp; }
    public Priority getPriority() { return Priority.fromInstructionType(getInstructionType()); }


    public void setInstructionType(String instructionType) {
        this.instructionType = instructionType;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public void setUOM(int UOM) {
        this.UOM = UOM;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String toString() {
        return "InstructionMessage " +
                 instructionType +
                " "+ productCode +
                " " + quantity +
                " " + UOM +
                " "  + timestamp;
    }
}
