package ytodo.org;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class InstructionQueueTest {

    private InstructionQueue instructionQueue;

    @BeforeEach
    void setUp() {
        instructionQueue = new InstructionQueue();
    }

    @Test
    void testEnqueue() throws Exception {
        instructionQueue.enqueue("InstructionMessage A PK31 100 2 2024-01-01T12:04:25.012Z");
        assertEquals(1, instructionQueue.count());
        assertEquals("A", instructionQueue.peek().getInstructionType());
    }

    @Test
    void testPeek() throws Exception {
        instructionQueue.enqueue("InstructionMessage C PP43 50 1 2022-10-01T10:01:33.012Z");
        instructionQueue.enqueue("InstructionMessage A DF10 100 2 2024-03-01T10:01:33.012Z");
        assertEquals("A", instructionQueue.peek().getInstructionType());
    }

    @Test
    void testPeekShouldReturnFirstMessageInIfMessagesHaveSameType() throws Exception {
        instructionQueue.enqueue("InstructionMessage C PP43 50 1 2022-10-01T10:01:33.012Z");
        instructionQueue.enqueue("InstructionMessage C PP53 70 3 2024-05-01T10:01:33.012Z");
        assertEquals("2022-10-01T10:01:33.012Z", instructionQueue.peek().getTimestamp());
    }

    @Test
    void testDequeue() throws Exception {
        instructionQueue.enqueue("InstructionMessage B FP44 100 2 2024-01-01T10:01:33.012Z");
        instructionQueue.enqueue("InstructionMessage C PQ38 50 1 2024-06-01T10:01:33.012Z");
        assertEquals("B", instructionQueue.dequeue().getInstructionType());
        assertEquals(1, instructionQueue.count());
    }

    @Test
    void testDequeueShouldRemoveFirstMessageInIfMessagesHaveSameType() throws Exception {
        instructionQueue.enqueue("InstructionMessage C PP43 50 1 2022-10-01T10:01:33.012Z");
        instructionQueue.enqueue("InstructionMessage C PP53 70 3 2024-05-01T10:01:33.012Z");
        String expectedTimeStamp = "2022-10-01T10:01:33.012Z";
        assertEquals(2, instructionQueue.count());

        InstructionMessage dequeuedMessage = instructionQueue.dequeue();

        assertEquals(1, instructionQueue.count());
        assertEquals(expectedTimeStamp, dequeuedMessage.getTimestamp());
    }

    @Test
    void testIsEmpty() {
        assertTrue(instructionQueue.isEmpty());
    }

    @Test
    void testCount() throws Exception {
        instructionQueue.enqueue("InstructionMessage A PP11 100 2 2021-10-01T10:01:33.012Z");
        instructionQueue.enqueue("InstructionMessage B PP12 80 1 2020-09-01T09:01:39.022Z");
        assertEquals(2, instructionQueue.count());
    }

}