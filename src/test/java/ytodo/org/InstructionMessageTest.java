package ytodo.org;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class InstructionMessageTest {

    private InstructionMessage instructionMessage;

    @BeforeEach
    void setUp() {
        instructionMessage = new InstructionMessage();
    }

    @Test
    void testInstructionType() {
        String instructionType = "C";
        instructionMessage.setInstructionType(instructionType);
        assertEquals(instructionType, instructionMessage.getInstructionType());
        assertEquals(InstructionMessage.Priority.LOW, instructionMessage.getPriority());
    }

    @Test
    void testProductCode() {
        String productCode = "XYZ-123";
        instructionMessage.setProductCode(productCode);
        assertEquals(productCode, instructionMessage.getProductCode());
    }

    @Test
    void testQuantity() {
        int quantity = 5;
        instructionMessage.setQuantity(quantity);
        assertEquals(quantity, instructionMessage.getQuantity());
    }

    @Test
    void testUOM() {
        int UOM = 100;
        instructionMessage.setUOM(UOM);
        assertEquals(UOM, instructionMessage.getUOM());
    }

    @Test
    void testTimestamp() {
        String timestamp = "2024-09-25 12:00:00";
        instructionMessage.setTimestamp(timestamp);
        assertEquals(timestamp, instructionMessage.getTimestamp());
    }

    @Test
    void testToString() {
        String instructionType = "A";
        String productCode = "XYZ-123";
        int quantity = 5;
        int UOM = 100;
        String timestamp = "2024-09-25 12:00:00";

        instructionMessage.setInstructionType(instructionType);
        instructionMessage.setProductCode(productCode);
        instructionMessage.setQuantity(quantity);
        instructionMessage.setUOM(UOM);
        instructionMessage.setTimestamp(timestamp);

        String expected = "InstructionMessage " + instructionType + " " +
                productCode + " " + quantity + " " + UOM + " " + timestamp;

        assertEquals(expected, instructionMessage.toString());
    }
}