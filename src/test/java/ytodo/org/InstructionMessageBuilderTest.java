package ytodo.org;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class InstructionMessageBuilderTest {

    @Test
    void testMessageBuilder() {
        String instructionType = "B";
        String productCode = "ABC-123";
        int quantity = 10;
        int UOM = 200;
        String timestamp = "2024-10-01 12:30:00";

        InstructionMessage message = new InstructionMessageBuilder()
                .withInstructionType(instructionType)
                .withProductCode(productCode)
                .withQuantity(quantity)
                .withUOM(UOM)
                .withTimestamp(timestamp)
                .build();

        assertEquals(instructionType, message.getInstructionType());
        assertEquals(productCode, message.getProductCode());
        assertEquals(quantity, message.getQuantity());
        assertEquals(UOM, message.getUOM());
        assertEquals(timestamp, message.getTimestamp());
        assertEquals(InstructionMessage.Priority.MEDIUM, message.getPriority());
    }

}