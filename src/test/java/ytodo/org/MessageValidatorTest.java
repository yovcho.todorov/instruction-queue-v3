package ytodo.org;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MessageValidatorTest {

    private final MessageValidator validator = new MessageValidator();

    @Test
    void testIsValidInstructionType() {
        assertTrue(MessageValidator.isValidInstructionType("A"));
        assertFalse(MessageValidator.isValidInstructionType("E"));
    }

    @Test
    void testIsValidProductCode() {
        assertTrue(MessageValidator.isValidProductCode("AB12"));
        assertFalse(MessageValidator.isValidProductCode("A12B"));
    }

    @Test
    void testIsValidQuantity() {
        assertTrue(MessageValidator.isValidQuantity("1"));
        assertFalse(MessageValidator.isValidQuantity("-1"));
        assertFalse(MessageValidator.isValidQuantity("NaN"));
    }

    @Test
    void testIsValidUOM() {
        assertTrue(MessageValidator.isValidUOM("0"));
        assertTrue(MessageValidator.isValidUOM("255"));
        assertFalse(MessageValidator.isValidUOM("-1"));
        assertFalse(MessageValidator.isValidUOM("256"));
        assertFalse(MessageValidator.isValidUOM("NaN"));
    }

    @Test
    void testIsValidTimestamp() {
        assertTrue(MessageValidator.isValidTimestamp("2024-01-25T12:30:00.000Z"));
        assertFalse(MessageValidator.isValidTimestamp("2024-01-25 12:30:00"));
    }

    @Test
    void testValidateMessage() {
        InstructionMessage message = new InstructionMessage();
        message.setInstructionType("A");
        message.setProductCode("AB12");
        message.setQuantity(1);
        message.setUOM(0);
        message.setTimestamp("2024-03-27T12:30:00.000Z");

        assertDoesNotThrow(() -> validator.validateMessage(message));

        message.setInstructionType("E");
        Exception exception = assertThrows(IllegalArgumentException.class, () -> validator.validateMessage(message));
        assertEquals("!!!! Invalid instruction type: E", exception.getMessage());

        message.setInstructionType("D");
        message.setProductCode("Aa12");
        exception = assertThrows(IllegalArgumentException.class, () -> validator.validateMessage(message));
        assertEquals("!!!! Invalid product code: Aa12", exception.getMessage());

        message.setProductCode("AA12");
        message.setQuantity(-1);
        exception = assertThrows(IllegalArgumentException.class, () -> validator.validateMessage(message));
        assertEquals("!!!! Invalid quantity: -1", exception.getMessage());

        message.setQuantity(100);
        message.setUOM(-1);
        exception = assertThrows(IllegalArgumentException.class, () -> validator.validateMessage(message));
        assertEquals("!!!! Invalid UOM: -1", exception.getMessage());

        message.setUOM(256);
        exception = assertThrows(IllegalArgumentException.class, () -> validator.validateMessage(message));
        assertEquals("!!!! Invalid UOM: 256", exception.getMessage());

        message.setUOM(0);
        message.setTimestamp("2024-01-25 12:30:00");
        exception = assertThrows(IllegalArgumentException.class, () -> validator.validateMessage(message));
        assertEquals("!!!! Invalid timestamp: 2024-01-25 12:30:00", exception.getMessage());

        message.setTimestamp("1965-01-25 12:30:00T12:30:00.000Z");
        exception = assertThrows(IllegalArgumentException.class, () -> validator.validateMessage(message));
        assertEquals("!!!! Invalid timestamp: 1965-01-25 12:30:00T12:30:00.000Z", exception.getMessage());

        message.setTimestamp("2025-01-25 12:30:00T12:30:00.000Z");
        exception = assertThrows(IllegalArgumentException.class, () -> validator.validateMessage(message));
        assertEquals("!!!! Invalid timestamp: 2025-01-25 12:30:00T12:30:00.000Z", exception.getMessage());
    }

}