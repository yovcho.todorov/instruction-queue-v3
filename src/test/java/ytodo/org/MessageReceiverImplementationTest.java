package ytodo.org;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import static org.mockito.Mockito.times;

@ExtendWith(MockitoExtension.class)
class MessageReceiverImplementationTest {

    @Mock
    private InstructionQueue instructionQueue;

    private MessageReceiverImplementation messageReceiver;

    @BeforeEach
    void setUp() {
        messageReceiver = new MessageReceiverImplementation(instructionQueue);
    }

    @Test
    void testReceive() throws Exception {
        String message = "InstructionMessage A QW21 100 2 2024-10-01T09:04:10.012Z";
        String anotherMessage = "InstructionMessage A QW21 100 2 2024-10-01T09:04:10.012Z";
        messageReceiver.receive(message);
        messageReceiver.receive(anotherMessage);
        Mockito.verify(instructionQueue, times(2)).enqueue(message);
    }

}